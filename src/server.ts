import express, { Request, Response, NextFunction, Application } from 'express';

import { CurrencyConversionController } from './controllers';

const app: Application = express();
const port = process.env.PORT || 3000;

// Currency Conversion
app.use('/convert', CurrencyConversionController);

// Error Handeling
// Custom Error
class CustomError extends Error {
  status: number;
  constructor(message: string, status: number) {
    super(message);
    this.status = status;
  }
}
app.use((req: Request, res: Response, next: NextFunction) => {
  const error = new CustomError('Not Found', 404);
  next(error);
});

// Error from other sources
app.use(
  (error: CustomError, req: Request, res: Response, next: NextFunction) => {
    res.status(error.status || 500).json({
      error: error.message
    });
  }
);

// Initialising server
app.listen(port, () => {
  console.log(`Listening to http://localhost:${port}`);
});
