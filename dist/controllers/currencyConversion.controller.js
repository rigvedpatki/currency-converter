"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const axios_1 = __importDefault(require("axios"));
// Creating new route
const router = express_1.Router();
// GET /covert?from=xxx&to=xxx&amount=xxx
router.get('/', (req, res) => {
    let { from, to, amount } = req.query;
    if (from === undefined || to === undefined) {
        res.status(400).json({
            msg: 'Incomplete Query'
        });
    }
    else {
        axios_1.default
            .get(`https://api.exchangeratesapi.io/latest?symbols=${from},${to}`)
            .then(response => {
            console.log(response.data);
            let listOfCurrencies = response.data;
            let fromRate = listOfCurrencies.rates[from];
            let toRate = listOfCurrencies.rates[to];
            let exchangeRate;
            let exchangeAmount;
            console.log('from', fromRate);
            console.log('to', toRate);
            if (fromRate === undefined && toRate === undefined) {
                res.status(404).json({ msg: 'Unable to find the currency symbol' });
            }
            else if (fromRate === undefined || toRate === undefined) {
                if (from === 'EUR') {
                    fromRate = 1;
                }
                else if (to === 'EUR') {
                    toRate = 1;
                }
                else {
                    res.status(404).json({ msg: 'Unable to find the currency symbol' });
                }
            }
            console.log('from', fromRate);
            console.log('to', toRate);
            exchangeRate = toRate / fromRate;
            console.log('exchangeRate', exchangeRate);
            if (amount) {
                exchangeAmount = exchangeRate * amount;
                res.status(200).send({
                    msg: `Currency conversion rate for ${amount} ${from} to ${to} is ${exchangeAmount} with an exchange rate of ${exchangeRate} `,
                    exchangeRate: exchangeRate,
                    exchangeAmount: exchangeAmount,
                    date: response.data.date
                });
            }
            else {
                res.status(200).send({
                    msg: `Currency conversion rate for 1 ${from} to ${to} is ${exchangeRate} `,
                    exchangeRate: exchangeRate,
                    date: response.data.date
                });
            }
        })
            .catch(error => {
            res.status(500).json({ msg: 'Internal Error occured', error: error });
        });
    }
});
exports.CurrencyConversionController = router;
//# sourceMappingURL=currencyConversion.controller.js.map