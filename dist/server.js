"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controllers_1 = require("./controllers");
const app = express_1.default();
const port = process.env.PORT || 3000;
// Currency Conversion
app.use('/convert', controllers_1.CurrencyConversionController);
// Error Handeling
// Custom Error
class CustomError extends Error {
    constructor(message, status) {
        super(message);
        this.status = status;
    }
}
app.use((req, res, next) => {
    const error = new CustomError('Not Found', 404);
    next(error);
});
// Error from other sources
app.use((error, req, res, next) => {
    res.status(error.status || 500).json({
        error: error.message
    });
});
// Initialising server
app.listen(port, () => {
    console.log(`Listening to http://localhost:${port}`);
});
//# sourceMappingURL=server.js.map