## Currency Converter

Currency conversion REST API using Node.JS, Express JS and TypeScript.

# Possible Requests to the REST API

```
GET /convert?from=EUR&to=SEK&amount=100

Response:
{
    "msg": "Currency conversion rate for 100 EUR to SEK is 1059.1 with an exchange rate of 10.591 ",
    "exchangeRate": 10.591,
    "exchangeAmount": 1059.1,
    "date": "2018-09-06"
}
```

```
GET /convert?from=EUR&to=SEK

Response:
{
    "msg": "Currency conversion rate for 1 EUR to SEK is 10.591 ",
    "exchangeRate": 10.591,
    "date": "2018-09-06"
}
```

```
GET /convert?to=INR

Response:
{
    "msg": "Incomplete Query"
}
```

```
GET /convert?from=EUR

Response:
{
    "msg": "Incomplete Query"
}
```

```
GET /convert?from=ABC&to=USD

Response:
{
    "msg": "Unable to find the currency symbol"
}
```

# Deployed on Heroku

https://currency-conversion-api.herokuapp.com/
